const { Client } = require('pg');

class GameDB{
    #email = 'steven.lie@mail.com';
    #password = 'abc123';
    constructor(userObj){
        this.db = '';
        this.client = new Client({
            user: 'postgres',
            host: 'localhost',
            password: 'gajahsql',
            port: 5432,
        });
        this.authenticated = this.#authenticate(userObj);
    }
    #authenticate(userObj){
        return (this.#email === userObj.email) && (this.#password === userObj.password);
    }
    #useDb() {
        this.client = new Client({
        user: 'postgres',
        host: 'localhost',
        password: 'gajahsql',
        port: 5432,
        database: this.db,
        });
    }
    async #connectDb(){
        await this.client.connect()
            .then(() => console.log('connected'))
            .catch(err => console.error('connection error', err.stack));
    }
    async #disconnectDb(){
        await this.client.end()
        .then(() => console.log('client has disconnected \n'))
        .catch(err => console.error('error during disconnection \n', err.stack));
    }
    async createDatabase(db_name){
        if(this.authenticated){
            await this.#connectDb();
            await this.client.query(`CREATE DATABASE ${db_name}`)
            .then(() => {
                console.log(`${db_name} has been successfully created`);
                this.db = db_name;
            })
            .catch(e => console.error(e.stack));
            await this.#disconnectDb();
        }
        return 'Anda tidak memiliki hak';
    }
    async createTable(){
        if(this.authenticated){
            this.#useDb();
            await this.#connectDb();
            await this.client.query(`
                CREATE TABLE user_game(
                    user_id INT PRIMARY KEY,
                    username VARCHAR(50) NOT NULL,
                    password VARCHAR(50) NOT NULL,
                    email VARCHAR(50) NOT NULL
                ); 
                CREATE TABLE user_game_biodata(
                    biodata_id INT PRIMARY KEY,
                    user_id INT NOT NULL,
                    user_country VARCHAR(50) NOT NULL,
                    user_rank VARCHAR(50) NOT NULL,
                    gold_amount VARCHAR(50) NOT NULL,
                    FOREIGN KEY(user_id)
                        REFERENCES user_game(user_id)
                );
                CREATE TABLE user_game_history(
                    history_id INT PRIMARY KEY,
                    user_id INT NOT NULL,
                    match_duration TIME NOT NULL,
                    match_score INT NOT NULL,
                    FOREIGN KEY(user_id)
                        REFERENCES user_game(user_id)
                );
            `)
            .then(res => {
                console.log(`Table has been successfully created`);
                return res;
            })
            .catch(e => console.error(e.stack));
            await this.#disconnectDb();
        }
        return 'Anda tidak memiliki hak';
    }
    async insertData(){
        if(this.authenticated){
            this.#useDb();
            await this.#connectDb();
            await this.client.query(`
                INSERT INTO user_game(user_id, username, password, email)
                VALUES(1, 'steve', 'gemink123', 'steve@gmail.com');
                INSERT INTO user_game_biodata(biodata_id, user_id, user_country, user_rank, gold_amount)
                VALUES(1, 1, 'Indonesia', 'legendary', 10000);
                INSERT INTO user_game_history(history_id, user_id, match_duration, match_score)
                VALUES(1, 1, '00:25:00', 90),
                (2, 1, '00:18:00', 75),
                (3, 1, '00:22:00', 80);
            `)
            .then((res) => console.log('Insert data successful'))
            .catch(e => console.error(e.stack));
            await this.#disconnectDb();
        }
        return 'Anda tidak memiliki hak';
    }
    async readData(){
        if(this.authenticated){
            this.#useDb();
            await this.#connectDb();
            await this.client.query(`
                SELECT * FROM user_game 
                JOIN user_game_biodata ON user_game.user_id = user_game_biodata.user_id 
                JOIN user_game_history ON user_game.user_id = user_game_history.user_id;
            `)
            .then((res) => console.log(res['rows'][0]))
            .catch(e => console.error(e.stack));
            await this.#disconnectDb();
        }
        return 'Anda tidak memiliki hak';
    }
    async updateData(){
        if(this.authenticated){
            this.#useDb();
            await this.#connectDb();
            await this.client.query(`
                UPDATE user_game 
                SET password = 'onepunchgame123';
            `)
            .then((res) => console.log(`updated ${res['rowCount']} row(s) `))
            .catch(e => console.error(e.stack));
            await this.#disconnectDb();
        }
        return 'Anda tidak memiliki hak';
    }
    async deleteData(){
        if(this.authenticated){
            this.#useDb();
            await this.#connectDb();
            await this.client.query(`
                DELETE FROM user_game_history
                WHERE user_id = 1;
            `)
            .then((res) => console.log(`deleted ${res['rowCount']} row(s) `))
            .catch(e => console.error(e.stack));
            await this.#disconnectDb();
        }
        return 'Anda tidak memiliki hak';
    }
}

const crud_db = async () => {
    //1. create object game_db
    const game_db = new GameDB({email: 'steven.lie@mail.com', password: 'abc123'});
    //2. create database game_db
    await game_db.createDatabase('game_db');
    //3. create tables 
    await game_db.createTable();
    //4. insert data to every tables
    await game_db.insertData();
    //5. read / select from database with join
    await game_db.readData();
    //6. update data to user_game table
    await game_db.updateData();
    //7. delete data from user_history_game
    await game_db.deleteData();
}
crud_db();