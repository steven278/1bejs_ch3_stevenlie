# Challenge chapter 3 Binar Academy Backend Javascript

## 1. Clone project

## 2. install node-postgres

```js
npm install pg
```

## 3. Run all method immediately or run it one by one 

----

# db_game Database structure
## user_game Entity
- `primary key` : user_id
- `foreign key` : -
- `attributes`  : user_id, username, password, email
- `relation  `  : one to one with user_game_biodata and one to many with user_game_history

## user_game_biodata Entity

- `primary key` : biodata_id
- `foreign key` : user_id
- `attributes`  : biodata_id, user_id, user_country, user_rank, gold_amount
- `relation `   : one to one with user_game

## user_game_history Entity 
- `primary key` : history_id
- `foreign key` : user_id
- `attributes`  : history_id, user_id, match_duration, match_score
- `relation `   : many to one with user_game
